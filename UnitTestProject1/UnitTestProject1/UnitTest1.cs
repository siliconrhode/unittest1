﻿using System;
using System.IO;
using System.Reflection;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]

        // refactor code for different environments 
        public void TestMethod1()
        {
            var driver =  GetChromeDriver();
            driver.Navigate().GoToUrl("https://www.google.com");
            
        }

        private IWebDriver GetChromeDriver()
        {
            // dynamically locate the executing assembly path
            var outPutDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            return new ChromeDriver(outPutDirectory);
        }
    }
}
